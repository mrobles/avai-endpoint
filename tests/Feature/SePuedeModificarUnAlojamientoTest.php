<?php

namespace Tests\Feature;

use App\Models\Alojamiento;
use Illuminate\Http\Response;
use Tests\TestCase;

class SePuedeModificarUnAlojamientoTest extends TestCase
{

    public function test_se_puede_editar_alojamiento()
    {

        $alojamiento = Alojamiento::first();

        $user_id = $alojamiento->user_id;
        $id      = $alojamiento->accommodation_id;

        $data = [
            "id"           => $id,
            "trade_name"   => "Lujoso apartamento en la playa",
            "type"         => "FLAT",
            "distribution" => [
                "living_rooms" => 1,
                "bedrooms"     => 2,
                "beds"         => 3
            ],
            "max_guests"   => 3
        ];

        $response = $this->putJson(sprintf('/user/%s/accommodations/%s', $user_id, $id), $data);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertExactJson([
            "id"           => $id,
            "trade_name"   => "Lujoso apartamento en la playa",
            "type"         => "FLAT",
            "distribution" => [
                "living_rooms" => 1,
                "bedrooms"     => 2,
                "beds"         => 3
            ],
            "max_guests"   => 3,
            "updated_at"   => now()->format('Y-m-d')
        ]);
    }


    public function test_falla_al_editar_alojamiento_sin_datos()
    {
        $alojamiento = Alojamiento::first();

        $user_id = $alojamiento->user_id;
        $id      = $alojamiento->accommodation_id;

        $response = $this->putJson(sprintf('/user/%s/accommodations/%s', $user_id, $id));

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }


    public function test_falla_al_editar_alojamiento_superando_huespedes_maximos()
    {
        $alojamiento = Alojamiento::first();

        $user_id = $alojamiento->user_id;
        $id      = $alojamiento->accommodation_id;

        $data = [
            "id"           => $id,
            "trade_name"   => "Lujoso apartamento en la playa",
            "type"         => "FLAT",
            "distribution" => [
                "living_rooms" => 1,
                "bedrooms"     => 2,
                "beds"         => 3
            ],
            "max_guests"   => 4
        ];

        $response = $this->putJson(sprintf('/user/%s/accommodations/%s', $user_id, $id), $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }


    public function test_falla_al_editar_alojamiento_sin_nombre()
    {
        $alojamiento = Alojamiento::first();

        $user_id = $alojamiento->user_id;
        $id      = $alojamiento->accommodation_id;

        $data = [
            "id"           => $id,
            "trade_name"   => "",
            "type"         => "FLAT",
            "distribution" => [
                "living_rooms" => 1,
                "bedrooms"     => 2,
                "beds"         => 3
            ],
            "max_guests"   => 3
        ];

        $response = $this->putJson(sprintf('/user/%s/accommodations/%s', $user_id, $id), $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }


    public function test_falla_al_editar_alojamiento_con_nombre_largo()
    {
        $alojamiento = Alojamiento::first();

        $user_id = $alojamiento->user_id;
        $id      = $alojamiento->accommodation_id;

        $data = [
            "id"           => $id,
            "trade_name"   => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tristique nunc quis ligula blandit viverra. Suspendisse posuere risus vel dui cras amet.",
            "type"         => "FLAT",
            "distribution" => [
                "living_rooms" => 1,
                "bedrooms"     => 2,
                "beds"         => 3
            ],
            "max_guests"   => 3
        ];

        $response = $this->putJson(sprintf('/user/%s/accommodations/%s', $user_id, $id), $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }


    public function test_falla_al_editar_alojamiento_con_tipo_invalido()
    {
        $alojamiento = Alojamiento::first();

        $user_id = $alojamiento->user_id;
        $id      = $alojamiento->accommodation_id;

        $data = [
            "id"           => $id,
            "trade_name"   => "Lujoso apartamento en la playa",
            "type"         => "XLT",
            "distribution" => [
                "living_rooms" => 1,
                "bedrooms"     => 2,
                "beds"         => 3
            ],
            "max_guests"   => 3
        ];

        $response = $this->putJson(sprintf('/user/%s/accommodations/%s', $user_id, $id), $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }


    public function test_falla_al_editar_alojamiento_de_otro_usuario()
    {
        $alojamiento = Alojamiento::first();

        $user_id = $alojamiento->user_id;
        $id      = $alojamiento->id + 1;

        $data = [
            "id"           => $id,
            "trade_name"   => "Lujoso apartamento en la playa",
            "type"         => "XLT",
            "distribution" => [
                "living_rooms" => 1,
                "bedrooms"     => 2,
                "beds"         => 3
            ],
            "max_guests"   => 3
        ];

        $response = $this->putJson(sprintf('/user/%s/accommodations/%s', $user_id, $id), $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }
}
