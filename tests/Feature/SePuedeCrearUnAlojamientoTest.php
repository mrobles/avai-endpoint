<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

class SePuedeCrearUnAlojamientoTest extends TestCase
{

    public function test_se_puede_crear_alojamiento()
    {
        $user_id = 1;

        $data = [
            "trade_name"   => "Lujoso apartamento en la playa",
            "type"         => "FLAT",
            "distribution" => [
                "living_rooms" => 1,
                "bedrooms"     => 2,
                "beds"         => 3
            ],
            "max_guests"   => 3
        ];

        $response = $this->postJson(sprintf('/user/%s/accommodations', $user_id), $data);

        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJsonFragment([
            "trade_name"   => "Lujoso apartamento en la playa",
            "type"         => "FLAT",
            "distribution" => [
                "living_rooms" => 1,
                "bedrooms"     => 2,
                "beds"         => 3
            ],
            "max_guests"   => 3,
            "updated_at"   => now()->format('Y-m-d')
        ]);
    }


    public function test_falla_al_crear_alojamiento_sin_datos()
    {
        $user_id = 1;

        $response = $this->postJson(sprintf('/user/%s/accommodations', $user_id));

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }


    public function test_falla_al_crear_alojamiento_superando_huespedes_maximos()
    {
        $user_id = 1;

        $data = [
            "trade_name"   => "Lujoso apartamento en la playa",
            "type"         => "FLAT",
            "distribution" => [
                "living_rooms" => 1,
                "bedrooms"     => 2,
                "beds"         => 3
            ],
            "max_guests"   => 4
        ];

        $response = $this->postJson(sprintf('/user/%s/accommodations', $user_id), $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }


    public function test_falla_al_crear_alojamiento_sin_nombre()
    {
        $user_id = 1;

        $data = [
            "trade_name"   => "",
            "type"         => "FLAT",
            "distribution" => [
                "living_rooms" => 1,
                "bedrooms"     => 2,
                "beds"         => 3
            ],
            "max_guests"   => 3
        ];

        $response = $this->postJson(sprintf('/user/%s/accommodations', $user_id), $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }


    public function test_falla_al_crear_alojamiento_con_nombre_largo()
    {
        $user_id = 1;

        $data = [
            "trade_name"   => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tristique nunc quis ligula blandit viverra. Suspendisse posuere risus vel dui cras amet.",
            "type"         => "FLAT",
            "distribution" => [
                "living_rooms" => 1,
                "bedrooms"     => 2,
                "beds"         => 3
            ],
            "max_guests"   => 3
        ];

        $response = $this->postJson(sprintf('/user/%s/accommodations', $user_id), $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function test_falla_al_crear_alojamiento_con_tipo_invalido()
    {
        $user_id = 1;

        $data = [
            "trade_name"   => "Lujoso apartamento en la playa",
            "type"         => "XLT",
            "distribution" => [
                "living_rooms" => 1,
                "bedrooms"     => 2,
                "beds"         => 3
            ],
            "max_guests"   => 3
        ];

        $response = $this->postJson(sprintf('/user/%s/accommodations', $user_id), $data);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

}
