<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

class SePuedeConsultarUnAlojamientoTest extends TestCase
{
    public function test_se_pueden_consultar_alojamientos_de_usuario()
    {
        $user_id = 1;

        $response = $this->getJson(sprintf('/user/%s/accommodations',$user_id));

        $response->assertStatus(Response::HTTP_OK);
    }
}
