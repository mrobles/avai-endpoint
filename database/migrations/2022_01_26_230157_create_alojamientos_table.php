<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlojamientosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accommodations', function (Blueprint $table) {
            $table->string('user_name')->nullable();
            $table->string('user_id')->index(); // Existen IDs no numericos aunque es un requisito (B9B,94B,...)
            $table->unsignedBigInteger('accommodation_id')->autoIncrement();
            $table->string('accommodation_name', 150);
            $table->string('accommodation_address')->nullable();
            $table->string('accommodation_city')->nullable();
            $table->string('accommodation_postal_code', 10)->nullable();
            $table->string('accommodation_country', 2)->nullable();
            $table->enum('accommodation_type', [ 'HOUSE', 'FLAT', 'VILLA' ]);
            $table->text('distribution'); // Esta version no permite json
            $table->unsignedMediumInteger('max_guests');
            $table->dateTime('last_update');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accommodations');
    }
}
