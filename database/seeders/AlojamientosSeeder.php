<?php

namespace Database\Seeders;

use App\Helper\Helper;
use App\Models\Alojamiento;
use Illuminate\Database\Seeder;

class AlojamientosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = storage_path("app/data.csv");
        $records = Helper::import_CSV($file);

        foreach ($records as $record) {
            Alojamiento::create($record);
        }
    }
}
