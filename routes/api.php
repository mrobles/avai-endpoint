<?php

use App\Http\Controllers\AlojamientosController;
use Illuminate\Support\Facades\Route;

Route::group([ 'middleware' => [ 'json.header' ] ], function () {
    Route::post('/user/{user_id}/accommodations', [ AlojamientosController::class, 'post' ]);
    Route::put('/user/{user_id}/accommodations/{id}', [ AlojamientosController::class, 'put' ]);
    Route::get('/user/{user_id}/accommodations', [ AlojamientosController::class, 'get' ]);

});
