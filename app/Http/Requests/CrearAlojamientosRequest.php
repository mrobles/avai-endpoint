<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class CrearAlojamientosRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "trade_name"                => "required|min:1|max:150",
            "type"                      => "required|in:HOUSE,FLAT,VILLA",
            "distribution.living_rooms" => "required|integer|min:1",
            "distribution.bedrooms"     => "required|integer|min:1",
            "distribution.beds"         => "required|integer|min:1",
            "max_guests"                => "required|integer|lte:distribution.beds",
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        abort(Response::HTTP_BAD_REQUEST);
    }
}
