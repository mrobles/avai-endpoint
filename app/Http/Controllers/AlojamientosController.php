<?php

namespace App\Http\Controllers;

use App\Dtos\AlojamientosDTO;
use App\Http\Requests\CrearAlojamientosRequest;
use App\Http\Requests\EditarAlojamientosRequest;
use App\Models\Alojamiento;
use Illuminate\Http\Response;

class AlojamientosController extends Controller
{

    /**
     * Para obtener todos los alojamientos de un propietario que cumplan las restricciones.
     *
     * @param $user_id
     *
     * @return mixed
     */
    public function get($user_id)
    {
        self::isValidNumberFormat($user_id);

        return response()->json(Alojamiento::where('user_id',
            $user_id)->weekend()->get()->mapInto(AlojamientosDTO::class), Response::HTTP_OK);

    }


    /**
     * Para crear un nuevo alojamiento. Recuerda que no hace falta persistir los datos, es opcional. La respuesta si es
     * obligatoria
     *
     * @param CrearAlojamientosRequest $request
     * @param                          $user_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function post(CrearAlojamientosRequest $request, $user_id)
    {
        self::isValidNumberFormat($user_id);

        $alojamiento = Alojamiento::create([
            'user_id'            => $user_id,
            'accommodation_name' => $request->trade_name,
            'accommodation_type' => $request->type,
            'distribution'       => json_encode($request->distribution),
            'max_guests'         => $request->max_guests,
            'last_update'        => now()
        ]);

        return response()->json($alojamiento->jsonSerialize(), Response::HTTP_CREATED);

    }


    /**
     * Para actualizar un alojamiento existente. Recuerda que no hace falta persistir los datos, es opcional. La
     * respuesta si es obligatoria
     *
     * @param EditarAlojamientosRequest $request
     * @param                           $user_id
     * @param                           $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function put(EditarAlojamientosRequest $request, $user_id, $id)
    {

        self::isValidNumberFormat($user_id);
        self::isValidNumberFormat($id);

        $alojamiento = Alojamiento::find($id);

        if (is_null($alojamiento)) {
            abort(Response::HTTP_BAD_REQUEST);
        }
        if ($alojamiento->user_id != $user_id) {
            abort(Response::HTTP_BAD_REQUEST);
        }

        $alojamiento->update([
            'user_id'            => $user_id,
            'accommodation_name' => $request->trade_name,
            'accommodation_type' => $request->type,
            'distribution'       => json_encode($request->distribution),
            'max_guests'         => $request->max_guests,
            'last_update'        => now()
        ]);

        $alojamiento->refresh();

        return response()->json($alojamiento->jsonSerialize(), Response::HTTP_OK);


    }


    /**
     * Comprueba si el parámetro introducido es numérico y lanza un 400 si no lo es
     *
     * @param $user_id
     */
    private static function isValidNumberFormat($user_id)
    {
        if ( ! is_numeric($user_id)) {
            abort(Response::HTTP_BAD_REQUEST);
        }
    }

}
