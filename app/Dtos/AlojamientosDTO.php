<?php

namespace App\Dtos;

use App\Models\Alojamiento;
use JsonSerializable;

class AlojamientosDTO implements JsonSerializable
{

    private Alojamiento $alojamiento;


    /**
     * AlojamientosDTO constructor.
     *
     * @param Alojamiento $alojamiento
     */
    public function __construct(Alojamiento $alojamiento)
    {
        $this->alojamiento = $alojamiento;
    }


    /**
     * Formatea listado de alojamientos
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->alojamiento->jsonSerialize();
    }
}
