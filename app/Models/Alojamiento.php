<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Alojamiento extends Model
{

    use HasFactory;

    public $timestamps = false;

    protected $table = "accommodations";

    protected $primaryKey = 'accommodation_id';

    protected $dates = [ 'last_update' ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name',
        'user_id',
        'accommodation_id',
        'accommodation_name',
        'accommodation_address',
        'accommodation_city',
        'accommodation_postal_code',
        'accommodation_country',
        'accommodation_type',
        'distribution',
        'max_guests',
        'last_update',
    ];


    /**
     * Filtra solo los fines de semana
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeWeekend($query)
    {
        return $query->where(function ($q) {
            $q->where(DB::raw('DAYOFWEEK(last_update)'), 7)->orWhere(DB::raw('DAYOFWEEK(last_update)'), 1);
        });
    }


    /**
     * Formatea  alojamientos
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            "id"           => $this->accommodation_id,
            "trade_name"   => $this->accommodation_name,
            "type"         => $this->accommodation_type,
            "distribution" => json_decode($this->distribution),
            "max_guests"   => $this->max_guests,
            "updated_at"   => $this->last_update->format('Y-m-d')
        ];
    }

}
